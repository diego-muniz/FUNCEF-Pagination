angular.module('funcef-pagination').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/pagination.view.html',
    "<div class=\"text-center\" st-pagination=\"\" st-template=\"views/template1.view.html\" st-items-by-page=\"filtro.pagination.number\"></div>"
  );


  $templateCache.put('views/template1.view.html',
    "<nav ng-if=\"pages.length >= 2\"> <ul class=\"pagination\"> <li> <a class=\"fa fa-backward\" ng-click=\"selectPage(1)\"></a> </li> <li> <a class=\"fa fa-caret-left fa-lg\" ng-click=\"selectPage(currentPage - 1)\"></a> </li> <li ng-repeat=\"page in pages\" ng-class=\"{active: page==currentPage}\"> <a href=\"javascript: void(0);\" ng-click=\"selectPage(page)\">{{page}}</a> </li> <li> <a class=\"block-input\"> <input type=\"number\" class=\"select-page\" min=\"1\" ng-model=\"inputPage\" ng-change=\"selectPage(inputPage)\"> de {{numPages | number: 0}} </a> </li> <li> <a class=\"fa fa-caret-right fa-lg\" ng-click=\"selectPage(currentPage + 1)\"></a> </li> <li> <a class=\"fa fa-forward\" ng-click=\"selectPage(numPages)\"></a> </li> </ul> </nav>"
  );

}]);
