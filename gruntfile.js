﻿// Generated on 2017-05-11 using generator-angular 0.16.0
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Automatically load required Grunt tasks
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin',
        ngtemplates: 'grunt-angular-templates',
        cdnify: 'grunt-google-cdn'
    });

    // Configurable paths for the application
    var appConfig = {
        app: require('./bower.json').appPath || 'app',
        demo: 'demo',
        target: 'target',
        dist: 'dist'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),
        // Project settings
        yeoman: appConfig,

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            bower: {
                files: ['bower.json'],
                tasks: ['wiredep']
            },
            js: {
                files: ['<%= yeoman.app %>/scripts/{,*/}*.js', '<%= yeoman.demo %>/scripts/{,*/}*.js'],
                tasks: ['newer:jshint:all', 'newer:jscs:all'],
                options: {
                    livereload: '<%= connect.options.livereload %>'
                }
            },
            styles: {
                files: ['<%= yeoman.app %>/styles/{,*/}*.css', '<%= yeoman.demo %>/styles/{,*/}*.css'],
                //tasks: ['newer:copy:styles', 'postcss']
                tasks: ['postcss']
            },
            gruntfile: {
                files: ['Gruntfile.js']
            },
            livereload: {
                options: {
                    livereload: '<%= connect.options.livereload %>'
                },
                files: [
                  //'<%= yeoman.app %>/{,*/}*.html',
                  '<%= yeoman.app %>/views/**/*.html',
                  '<%= yeoman.demo %>/views/**/*.html',
                  '.tmp/styles/{,*/}*.css',
                  '<%= yeoman.app %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                  '<%= yeoman.demo %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },

        // The actual grunt server settings
        connect: {
            options: {
                port: 9091,
                // Change this to '0.0.0.0' to access the server from outside.
                hostname: 'localhost',
                livereload: 35791
            },
            livereload: {
                options: {
                    open: true,
                    middleware: function (connect) {
                        return [
                          connect.static('.tmp'),
                          connect().use('/bower_components',
                            connect.static('./bower_components')
                          ),
                          connect().use('/demo/styles',
                            connect.static('./demo/styles')
                          ),
                          connect.static(appConfig.demo)
                        ];
                    }
                }
            },
            dist: {
                options: {
                    open: true,
                    base: '<%= yeoman.target %>'
                }
            }
        },

        // Make sure there are no obvious mistakes
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: require('jshint-stylish')
            },
            all: {
                src: [
                  'Gruntfile.js',
                  '<%= yeoman.app %>/scripts/{,*/}*.js',
                  '<%= yeoman.demo %>/scripts/{,*/}*.js'
                ]
            }
        },

        // Make sure code styles are up to par
        jscs: {
            options: {
                config: '.jscsrc',
                verbose: true
            },
            all: {
                src: [
                  'Gruntfile.js',
                  '<%= yeoman.app %>/scripts/{,*/}*.js',
                  '<%= yeoman.demo %>/scripts/{,*/}*.js'
                ]
            }
        },

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                      '.tmp',
                      '<%= yeoman.dist %>/{,*/}*',
                      '!<%= yeoman.dist %>/.git{,*/}*',
                      'bower_components/<%= pkg.name %>'
                    ]
                }]
            },
            server: '.tmp',
            options: {
                options: { force: true },
                deleteEmptyFolders: false
            }
        },

        // Add vendor prefixed styles
        postcss: {
            options: {
                processors: [
                  require('autoprefixer-core')({ browsers: ['last 1 version'] })
                ]
            },
            server: {
                options: {
                    map: true
                },
                files: [{
                    expand: true,
                    cwd: '.tmp/styles/',
                    src: '{,*/}*.css',
                    dest: '.tmp/styles/'
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/styles/',
                    src: '{,*/}*.css',
                    dest: '.tmp/styles/'
                }]
            }
        },

        // Automatically inject Bower components into the app
        wiredep: {
            dev: {
                devDependencies: true,
                src: ['<%= yeoman.demo %>/index.html'],
                ignorePath:  /\.\.\//
            },
            app: {
                src: ['<%= yeoman.demo %>/index.html'],
                ignorePath: /\.\.\//
            }
        },

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            html: '<%= yeoman.demo %>/index.html',
            options: {
                dest: '<%= yeoman.target %>',
                flow: {
                    html: {
                        steps: {
                            js: ['concat', 'uglifyjs'],
                            css: ['cssmin']
                        },
                        post: {}
                    }
                }
            }
        },

        // Performs rewrites based on filerev and the useminPrepare configuration
        usemin: {
            html: ['<%= yeoman.target %>/{,*/}*.html'],
            css: ['<%= yeoman.target %>/styles/{,*/}*.css'],
            js: ['<%= yeoman.target %>/scripts/{,*/}*.js'],
            options: {
                assetsDirs: [
                  '<%= yeoman.target %>',
                  '<%= yeoman.target %>/images',
                  '<%= yeoman.target %>/styles'
                ],
                patterns: {
                    js: [[/(images\/[^''""]*\.(png|jpg|jpeg|gif|webp|svg))/g, 'Replacing references to images']]
                }
            }
        },

        // The following *-min tasks will produce minified files in the dist folder
        // By default, your `index.html`'s <!-- Usemin block --> will take care of
        // minification. These next options are pre-configured if you do not wish
        // to use the Usemin blocks.
        cssmin: {
            dist: {
                files: {
                    'dist/<%= pkg.name %>.min.css': ['<%= yeoman.app %>/styles/*.css']
                }
            }
        },
        uglify: {
            dist: {
                files: {
                    'dist/<%= pkg.name %>.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },

        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: ['<%= yeoman.app %>/scripts/{,*/}*.js', '.tmp/*.js'],
                dest: 'dist/<%= pkg.name %>.js'
            },
            css: {
                src: ['<%= yeoman.app %>/styles/*.css'],
                dest: 'dist/<%= pkg.name %>.css',
            }
        },

        copy: {
            main: {
                files: [
                  {
                      expand: true,
                      src: ['dist/*'],
                      dest: 'bower_components/<%= pkg.name %>/'
                  },
                  {
                      'bower_components/<%= pkg.name %>/bower.json' : 'app/bower.json'
                  },
                ],
            },
        },

        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.demo %>/images',
                    src: '[*/*.{png,jpg,jpeg,gif,svg}]',
                    dest: '<%= yeoman.target %>/images'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.demo %>/images',
                    src: '{,*/}*.svg',
                    dest: '<%= yeoman.target %>/images'
                }]
            }
        },

        htmlmin: {
            target: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.target %>',
                    src: ['*.html'],
                    dest: '<%= yeoman.target %>'
                }]
            }
        },

        ngtemplates: {
            dist: {
                options: {
                    module: '<%= pkg.name %>',
                    htmlmin: '<%= htmlmin.target.options %>',
                    usemin: '<%= yeoman.dist %>/<%= pkg.name %>.js'
                },
                cwd: '<%= yeoman.app %>',
                src: [
                      'views/*.html'
                ],
                dest: '.tmp/templateCache.js'
            }
        },

        // ng-annotate tries to make the code safe for minification automatically
        // by using the Angular long form for dependency injection.
        ngAnnotate: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/concat/scripts',
                    src: '*.js',
                    dest: '.tmp/concat/scripts'
                }]
            }
        },

        // Replace Google CDN references
        cdnify: {
            target: {
                html: ['<%= yeoman.target %>/*.html']
            }
        },

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            target: [
              'copy:styles',
              'imagemin',
              'svgmin'
            ]
        }
    });


    grunt.registerTask('serve', 'Compile then start a connect web server', function (target) {
        grunt.task.run([
            'clean',
            'wiredep:dev',
            'postcss:server',
            'ngtemplates',
            'concat',
            'ngAnnotate',
            'uglify',
            'cssmin',
            'copy',
            // 'build',
            'connect:livereload',
            'watch'
        ]);
    });


    grunt.registerTask('build', [
            'clean',
            'wiredep',
            'postcss:server',
            'ngtemplates',
            'concat',
            'ngAnnotate',
            'uglify',
            'cssmin',
            'copy'
    ]);
};
