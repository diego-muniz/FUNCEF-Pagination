﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @name FuncefDemo
    * @version 1.0.0
    * @Componente para teste de componentes
    */
    angular.module('funcef-demo.controller', []);
    angular.module('funcef-demo.service', []);

    angular
        .module('funcef-demo', [
            'funcef-demo.controller',
            'funcef-demo.service',
            'ui.router',
            'smart-table',
            'funcef-pagination',
            'funcef-summary'
        ]);
})();