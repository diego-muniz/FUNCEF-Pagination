﻿(function () {
    'use strict';

    angular.module('funcef-demo')
        .config(function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.when('', '/');
            $urlRouterProvider.otherwise('/404');

            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: 'views/demo/example1.view.html',
                    controller: 'Example1Controller',
                    controllerAs: 'vm',
                })

                .state('404', {
                    url: '/404',
                    templateUrl: 'pages/_site/404.view.html',
                    notAuthenticate: true,
                    ncyBreadcrumb: {
                        label: '404'
                    }
                })
        });
})();