(function () {
    'use strict';

    angular.module('funcef-demo.service')
        .service('FiltroService', FiltroService);

    function FiltroService() {
        var service = {
            calculaPaginas: calculaPaginas
        };

        return service;

        ////////////

        function calculaPaginas(filtro, qtdRegistros) {
            filtro.pagination.numberOfPages = Math.ceil(qtdRegistros / filtro.pagination.number);
            filtro.pagination.totalItemCount = qtdRegistros;
        }
    };
})();