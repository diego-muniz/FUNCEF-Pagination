﻿(function () {
    'use strict';

    angular.module('funcef-demo.controller')
        .controller('Example1Controller', Example1Controller);

    Example1Controller.$inject = ['FiltroService'];

    /* @ngInject */
    function Example1Controller(FiltroService) {
        var vm = this;
        vm.pesquisaServidor = pesquisaServidor;

        function pesquisaServidor(filtro) {
            vm.filtro = filtro;
            buscar();
        }

        function buscar() {
        	vm.lista = [];
        	for (var i = vm.filtro.pagination.start; i < (vm.filtro.pagination.start + vm.filtro.pagination.number); i++) {
        		vm.lista.push({
        			id: i
        		});
        	}
        	FiltroService.calculaPaginas(vm.filtro, 200);
        }
    }
}());