﻿(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @version 1.0.0
    */
    angular.module('funcef-pagination.controller', []);
    angular.module('funcef-pagination.directive', []);

    angular
    .module('funcef-pagination', [
      'funcef-pagination.controller',
      'funcef-pagination.directive'
    ]);
})();