(function () {
    'use strict';

    angular
      .module('funcef-pagination.directive')
      .directive('ngfPagination', ngfPagination);

    ngfPagination.$inject = [];

    /* @ngInject */
    function ngfPagination() {
        return {
            restrict: 'EA',
            templateUrl: 'views/pagination.view.html',
            controller: 'NgfPaginationController',
            controllerAs: 'vm',
            scope: {
                filtro: '=',
                campo: '='
            },
            link: function (scope, element, attrs) {
                console.log(scope.filtro);

                if (scope.campo) {
                    element.removeClass('show-field-numeric')
                    .addClass('show-field-numeric');
                }
            }
        }
    }
})();
