(function () {
    'use strict';
    /**
    * @ngdoc overview
    * @version 1.0.0
    */
    angular.module('funcef-pagination.controller', []);
    angular.module('funcef-pagination.directive', []);

    angular
    .module('funcef-pagination', [
      'funcef-pagination.controller',
      'funcef-pagination.directive'
    ]);
})();;(function () {
    'use strict';

    angular.module('funcef-pagination.controller')
        .controller('NgfPaginationController', NgfPaginationController);

    NgfPaginationController.$inject = [];

    /* @ngInject */
    function NgfPaginationController() {
        var vm = this;
        
    };

}());;(function () {
    'use strict';

    angular
      .module('funcef-pagination.directive')
      .directive('ngfPagination', ngfPagination);

    ngfPagination.$inject = [];

    /* @ngInject */
    function ngfPagination() {
        return {
            restrict: 'EA',
            templateUrl: 'views/pagination.view.html',
            controller: 'NgfPaginationController',
            controllerAs: 'vm',
            scope: {
                filtro: '=',
                campo: '='
            },
            link: function (scope, element, attrs) {
                console.log(scope.filtro);

                if (scope.campo) {
                    element.removeClass('show-field-numeric')
                    .addClass('show-field-numeric');
                }
            }
        }
    }
})();
;angular.module('funcef-pagination').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/pagination.view.html',
    "<div class=\"text-center\" st-pagination=\"\" st-template=\"views/template1.view.html\" st-items-by-page=\"filtro.pagination.number\"></div>"
  );


  $templateCache.put('views/template1.view.html',
    "<nav ng-if=\"pages.length >= 2\"> <ul class=\"pagination\"> <li> <a class=\"fa fa-backward\" ng-click=\"selectPage(1)\"></a> </li> <li> <a class=\"fa fa-caret-left fa-lg\" ng-click=\"selectPage(currentPage - 1)\"></a> </li> <li ng-repeat=\"page in pages\" ng-class=\"{active: page==currentPage}\"> <a href=\"javascript: void(0);\" ng-click=\"selectPage(page)\">{{page}}</a> </li> <li> <a class=\"block-input\"> <input type=\"number\" class=\"select-page\" min=\"1\" ng-model=\"inputPage\" ng-change=\"selectPage(inputPage)\"> de {{numPages | number: 0}} </a> </li> <li> <a class=\"fa fa-caret-right fa-lg\" ng-click=\"selectPage(currentPage + 1)\"></a> </li> <li> <a class=\"fa fa-forward\" ng-click=\"selectPage(numPages)\"></a> </li> </ul> </nav>"
  );

}]);
