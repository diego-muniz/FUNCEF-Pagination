# Componente - FUNCEF-Pagination

## Conteúdos

1. [Descrição](#descrição)
2. [Instalação](#instalação)
3. [Script](#script)
4. [CSS](#css)
5. [Módulo](#módulo)
6. [Uso](#uso)
7. [Parâmetros](#parâmetros)
8. [Desinstalação](#desinstalação)


## Descrição

- Componente Pagination é utilizado para paginação de grids.

 
## Instalação:

### Bower

- Necessário executar o bower install e passar o nome do componente.

```
bower install funcef-pagination --save
```

## Script

```html
<script src="bower_components/funcef-pagination/dist/funcef-pagination.js"></script>
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## CSS  

```html
<link rel="stylesheet" href="bower_components/funcef-pagination/dist/funcef-pagination.css">
```

__ATENÇÃO:__ Esta linha já é adicionada automaticamente com o uso do grunt pelo wiredep

## Módulo

- Adicione funcef-pagination dentro do módulo do Sistema.

```js
    angular
        .module('funcef-demo', ['funcef-pagination']);
```

## Uso

```html
<ngf-pagination filtro="vm.filtro" campo="true"></ngf-pagination>
```

### Parâmetros:

- ngf-pagination (Obrigatório);

## Desinstalação:

```
bower uninstall funcef-pagination --save
```